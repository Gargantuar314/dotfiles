# Don't source if bash is not in interactive mode
[[ $- != *i* ]] && return

# Set colorful PS1 only on colorful terminals.
# dircolors --print-database uses its own built-in database
# instead of using /etc/DIR_COLORS.  Try to use the external file
# first to take advantage of user additions.  Use internal bash
# globbing instead of external grep binary.
safe_term=${TERM//[^[:alnum:]]/?}   # sanitize TERM
match_lhs=""
[[ -f ~/.dir_colors   ]] && match_lhs="${match_lhs}$(<~/.dir_colors)"
[[ -f /etc/DIR_COLORS ]] && match_lhs="${match_lhs}$(</etc/DIR_COLORS)"
[[ -z ${match_lhs}    ]] \
	&& type -P dircolors >/dev/null \
	&& match_lhs=$(dircolors --print-database)
[[ $'\n'${match_lhs} == *$'\n'"TERM "${safe_term}* ]] && use_color=true

# Enable colors for ls, etc.  Prefer ~/.dir_colors #64489
if type -P dircolors >/dev/null ; then
    if [[ -f ~/.dir_colors ]] ; then
        eval $(dircolors -b ~/.dir_colors)
    elif [[ -f /etc/DIR_COLORS ]] ; then
        eval $(dircolors -b /etc/DIR_COLORS)
    fi
fi

if [[ ${EUID} == 0 ]] ; then
    PS1='\[\033[01;31m\][\h\[\033[01;36m\] \W\[\033[01;31m\]]\$\[\033[00m\] '
else
    PS1='\[\033[01;32m\][\u@\h\[\033[01;37m\] \W\[\033[01;32m\]]\$\[\033[00m\] '
fi

unset safe_term match_lhs sh

xhost +local:root > /dev/null 2>&1

# export QT_SELECT=4

# see https://unix.stackexchange.com/questions/26842/mounting-usb-drive-that-is-not-recognized
export MTP_NO_PROBE="1"

################################### SETTINGS ###################################
shopt -s autocd  # Auto prepend 'cd' if just one file
shopt -s checkwinsize  # Resizing will redraw lines
shopt -s histappend  # Append instead of overwrite history
set -o vi  # Vi mode
set -o noclobber  # Prevent overwrite through redirect
stty -ixon  # Disable <C-S> <C-Q>

# Powerline
powerline-daemon -q
POWERLINE_BASH_CONTINUATION=1
POWERLINE_BASH_SELECT=1
source /usr/share/powerline/bindings/bash/powerline.sh

################################### ALIASES ####################################
alias grep='grep --color'  # Colorful
alias egrep='egrep --color'
alias fgrep='fgrep --color'
alias ls='ls -lAhF --color'  # List all, human-readable with classifiers
alias lx='ls -BX --color'  # Sort by extension
alias lz='ls -rS --color'  # Sort by size
alias lt='ls -rt --color'  # Sort by time
alias cp='cp -i'  # Prevent overwrite
alias mv='mv -i'
alias rm='rm -i'
alias mkdir='mkdir -p'  # Create directories recursively
alias df='df -h'  # Human-readable disk space
alias free='free -m'  # Memory in MB
alias more='less'  # less is better
alias p='poweroff'
alias cfg='git --git-dir=$HOME/.cfg/ --work-tree=$HOME'
alias red='systemctl --user disable redshift.service --now'
alias RED='systemctl --user enable redshift.service --now'

# For university
alias snip='nvim ~/.config/nvim/snippet/tex.lua'
alias tex='nvim ~/texmf/tex/latex/tienuni/tienuni.sty'
alias uni='cd ~/Documents/university/2024_WS'

alias at='cd ~/Documents/university/2024_WS/algtop1; nvim algtop1_notes.tex'
alias rt='cd ~/Documents/university/2024_WS/reptheo1; nvim reptheo1_notes.tex'
alias ag='cd ~/Documents/university/2024_WS/alggeo1; nvim alggeo1_notes.tex'

alias ct='cd ~/Documents/book-notes/cattheo; nvim cattheo_notes.tex'
alias ca='cd ~/Documents/book-notes/commalg'

################################### UTILITIES ##################################
# Print all available color escape sequences
colors() {
	local fgc bgc vals seq0

	printf "Color escapes are %s\n" '\e[${value};...;${value}m'
	printf "Values 30..37 are \e[33mforeground colors\e[m\n"
	printf "Values 40..47 are \e[43mbackground colors\e[m\n"
	printf "Value  1 gives a  \e[1mbold-faced look\e[m\n\n"

	# foreground colors
	for fgc in {30..37}; do
		# background colors
		for bgc in {40..47}; do
			fgc=${fgc#37} # white
			bgc=${bgc#40} # black

			vals="${fgc:+$fgc;}${bgc}"
			vals=${vals%%;}

			seq0="${vals:+\e[${vals}m}"
			printf "  %-9s" "${seq0:-(default)}"
			printf " ${seq0}TEXT\e[m"
			printf " \e[${vals:+${vals+$vals;}}1mBOLD\e[m"
		done
		echo; echo
	done
}

# Extract archives with: ex <file>
ex() {
    if [ -f $1 ] ; then
        case $1 in
          *.tar.bz2)   tar xjf $1   ;;
          *.tar.gz)    tar xzf $1   ;;
          *.bz2)       bunzip2 $1   ;;
          *.rar)       unrar x $1   ;;
          *.gz)        gunzip $1    ;;
          *.tar)       tar xf $1    ;;
          *.tbz2)      tar xjf $1   ;;
          *.tgz)       tar xzf $1   ;;
          *.zip)       unzip $1     ;;
          *.Z)         uncompress $1;;
          *.7z)        7z x $1      ;;
          *)           echo "'$1' cannot be extracted via ex()" ;;
        esac
    else
        echo "'$1' is not a valid file"
    fi
}

run() {
    if [ -f $1 ]; then
        clang++ -O2 -DNDEBUG -march=native $1 && ./a.out
    else
        echo "'$1' is not a valid file"
    fi
}
