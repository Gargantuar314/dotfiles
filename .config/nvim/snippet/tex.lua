-- Note: We avoid 'fmt' and 'fmta' for speed (these functions convert them to
-- the more verbose way anyway)

-- HELPER FUNCTIONS --------------------------------------------------------{{{1

local function insert_wrap(_, snip) -- Emulate Ultisnips' '${1:${VISUAL}}'
    local raw = snip.env.LS_SELECT_RAW
    if #raw == 0 then -- Insert text if no selection
        return sn(nil, i(1))
    else -- Else insert selection
        return sn(nil, t(raw))
    end
end

local function wrap_braces(args) -- Should string be wrapped in braces?
    local text = args[1][1]
    -- Empty strings, single letters and single commands without braces don't
    -- need to be wrapped
    -- Update: Semantically correct would be to always wrap them into braces,
    -- since it is a but, not a feature to wrap them without braces. I would do
    -- an exception for single characters
    return not string.match(text, '^.$')
end

local function mathzone()
    return vim.fn['vimtex#syntax#in_mathzone']() == 1
end

local function env(name)
    local environment = vim.fn['vimtex#env#is_inside'](name)
    return environment[1] > 0 and environment[2] > 0
end

local function item_enum()
    return env('itemize') or env('enumerate')
end

local function desc()
    return env('description')
end

local function bib()
    return env('thebibliography')
end

local function tikz()
    return env('tikzpicture')
end

local function algo()
    return env('algorithmic')
end

local function tikzcd()
    return mathzone() and env('tikzcd')
end

-- MANUALLY TRIGGERED SNIPPETS ---------------------------------------------{{{1

return {

s({
    trig = 'beg',
    name = 'begin/end environment',
    condition = conds.line_begin,
    show_condition = conds.line_begin
}, {
    t('\\begin{'), i(1), t('}'),
    n(2, '['), i(2), n(2, ']'),
    t({'', '\t'}), i(0),
    t({'', '\\end{'}), rep(1), t('}')
})

-- AUTOMATICALLY TRIGGERED SNIPPETS ----------------------------------------{{{1
 
}, {

s({
    trig = 'mt',
    name = 'inline math mode'
}, {
    t('\\('), d(1, insert_wrap), t('\\)')
}),

-- Math-mode Fonts ---------------------------------------------------------{{{1

s({
    trig = 'mrm',
    name = 'mathrm',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\mathrm{'), d(1, insert_wrap), t('}')
}),

s({
    trig = 'mbf',
    name = 'mathbf',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\mathbf{'), d(1, insert_wrap), t('}')
}),

s({
    trig = 'mtt',
    name = 'mathtt',
    priority = 1001, -- Clashes with 'tt'
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\mathtt{'), d(1, insert_wrap), t('}')
}),

s({
    trig = 'mcl',
    name = 'mathcal',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\mathcal{'), d(1, insert_wrap), t('}')
}),

s({
    trig = 'mbb',
    name = 'mathbb',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\mathbb{'), d(1, insert_wrap), t('}')
}),

s({
    trig = 'mfk',
    name = 'mathfrak',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\mathfrak{'), d(1, insert_wrap), t('}')
}),

s({
    trig = 'mbm',
    name = 'bold math',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\bm{'), d(1, insert_wrap), t('}')
}),

-- Normal fonts ------------------------------------------------------------{{{1

s({
    trig = 'fm',
    name = 'emph'
}, {
    t('\\emph{'), d(1, insert_wrap), t('}')
}),

s({
    trig = 'ft',
    name = 'textit'
}, {
    t('\\textit{'), d(1, insert_wrap), t('}')
}),

s({
    trig = 'ff',
    name = 'textbf'
}, {
    t('\\textbf{'), d(1, insert_wrap), t('}')
}),

s({
    trig = 'fc',
    name = 'textsc'
}, {
    t('\\textsc{'), d(1, insert_wrap), t('}')
}),

s({
    trig = 'fd',
    name = 'definition'
}, {
    t('\\defemph{'), d(1, insert_wrap), t('}')
}),

-- Headdings ---------------------------------------------------------------{{{1

s({
    trig = '#',
    name = 'section',
    condition = conds.line_begin,
    show_condition = conds.line_begin
}, {
    t('\\section{'), d(1, insert_wrap), t('}')
}),

s({
    trig = '##',
    name = 'subsection',
    condition = conds.line_begin,
    show_condition = conds.line_begin
}, {
    t('\\subsection{'), d(1, insert_wrap), t('}')
}),

s({
    trig = '###',
    name = 'subsubsection',
    condition = conds.line_begin,
    show_condition = conds.line_begin
}, {
    t('\\subsubsection{'), d(1, insert_wrap), t('}')
}),

s({
    trig = '####',
    name = 'paragraph',
    condition = conds.line_begin,
    show_condition = conds.line_begin
}, {
    t('\\paragraph{'), d(1, insert_wrap), t('}')
}),

s({
    trig = '#####',
    name = 'subparagraph',
    condition = conds.line_begin,
    show_condition = conds.line_begin
}, {
    t('\\subparagraph{'), d(1, insert_wrap), t('}')
}),

-- Lists -------------------------------------------------------------------{{{1

s({
    trig = 'item',
    name = 'itemize environment',
    condition = conds.line_begin,
    show_condition = conds.line_begin
}, {
    t({'\\begin{itemize}', '\t\\item '}),
    i(0),
    t({'', '\\end{itemize}'})
}),

s({
    trig = 'enum',
    name = 'enumerate environment',
    condition = conds.line_begin,
    show_condition = conds.line_begin
}, {
    t({'\\begin{enumerate}', '\t\\item '}),
    i(0),
    t({'', '\\end{enumerate}'})
}),

s({
    trig = 'desc',
    name = 'description environment',
    condition = conds.line_begin,
    show_condition = conds.line_begin
}, {
    t({'\\begin{description}', '\t\\item['}), i(1), t('] '),
    i(0),
    t('\\end{description}')
}),

s({
    trig = '    +',
    name = 'item',
    condition = function(...)
        return conds.line_begin(...) and item_enum(...)
    end,
    show_condition = function(...)
        return conds.line_begin(...) and item_enum(...)
    end
}, {
    t({'\\item '})
}),

s({
    trig = '    +',
    name = 'item',
    condition = function(...)
        return conds.line_begin(...) and desc(...)
    end,
    show_condition = function(...)
        return conds.line_begin(...) and desc(...)
    end
}, {
    t('\\item['), i(1), t('] ')
}),

s({
    trig = '+',
    name = 'bibitem',
    condition = function(...)
        return conds.line_begin(...) and bib(...)
    end,
    show_condition = function(...)
        return conds.line_begin(...) and bib(...)
    end
}, {
    t('\\bibitem'),
    n(2, '['), i(2), n(2, ']'),
    t('{'), i(1), t('}')
}),

-- Super-/subscript --------------------------------------------------------{{{1

s({
    trig = '_',
    name = 'subscript',
    wordTrig = false,
    condition = mathzone,
    show_condition = mathzone
}, {
    t('_'),
    m(1, wrap_braces, '{'), i(1), m(1, wrap_braces, '}')
}),

s({
    trig = '(%a)(%d)',
    name = 'subscript with digit',
    regTrig = true,
    condition = mathzone,
    show_condition = mathzone
}, {
    f(function(_, snip) return snip.captures[1] end),
    t('_'),
    f(function(_, snip) return snip.captures[2] end)
}),

s({
    trig = '^',
    name = 'superscript OR superscript with parenthesis',
    wordTrig = false,
    condition = mathzone,
    show_condition = mathzone
}, {
    t('^'),
    c(1, {
        sn(nil, { m(1, wrap_braces, '{'), i(1), m(1, wrap_braces, '}') }),
        sn(nil, { t('{('), i(1), t(')}') })
    })
}),

-- Accents -----------------------------------------------------------------{{{1

s({
    trig = ' sta',
    name = 'star',
    wordTrig = false,
    condition = mathzone,
    show_condition = mathzone
}, {
    t('^*')
}),

s({
    trig = ' crs',
    name = 'cross',
    wordTrig = false,
    condition = mathzone,
    show_condition = mathzone
}, {
    t('^\\times')
}),

s({
    trig = ' cir',
    name = 'circ',
    wordTrig = false,
    condition = mathzone,
    show_condition = mathzone
}, {
    t('^\\circ')
}),

s({
    trig = ' inv',
    name = 'inverse',
    wordTrig = false,
    condition = mathzone,
    show_condition = mathzone
}, {
    t('^{-1}')
}),

s({
    trig = ' sr',
    name = 'square',
    wordTrig = false,
    condition = mathzone,
    show_condition = mathzone
}, {
    t('^2')
}),

s({
    trig = 'hat',
    name = 'hat',
    wordTrig = false,
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\hat{'), d(1, insert_wrap), t('}')
}),

s({
    trig = 'Hat',
    name = 'widehat',
    wordTrig = false,
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\widehat{'), d(1, insert_wrap), t('}')
}),

s({
    trig = 'til',
    name = 'tilde',
    wordTrig = false,
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\tilde{'), d(1, insert_wrap), t('}')
}),

s({
    trig = 'Til',
    name = 'widetilde',
    wordTrig = false,
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\widetilde{'), d(1, insert_wrap), t('}')
}),

s({
    trig = 'vec',
    name = 'vector',
    wordTrig = false,
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\vec{'), d(1, insert_wrap), t('}')
}),

s({
    trig = 'bar',
    name = 'bar',
    wordTrig = false,
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\bar{'), d(1, insert_wrap), t('}')
}),

s({
    trig = 'cls',
    name = 'closure',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\close{'), d(1, insert_wrap), t('}')
}),

s({
    trig = 'opn',
    name = 'interior',
    priority = 1001,
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\open{'), d(1, insert_wrap), t('}')
}),

s({
    trig = 'cj',
    name = 'complex conjugate',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\conj{'), d(1, insert_wrap), t('}')
}),

s({
    trig = 'ovl',
    name = 'overline',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\overline{'), d(1, insert_wrap), t('}')
}),

s({
    trig = 'unl',
    name = 'underline',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\underline{'), d(1, insert_wrap), t('}')
}),

s({
    trig = 'ub',
    name = 'underbrace',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\underbrace{'), d(1, insert_wrap), t('}_'),
    m(2, wrap_braces, '{'), i(2), m(2, wrap_braces, '}')
}),

-- Math objects and operators ----------------------------------------------{{{1

s({
    trig = 'nmm',
    name = 'formatted number'
}, {
    t('\\num{'), d(1, insert_wrap), t('}')
}),

s({
    trig = 'qty',
    name = 'quantity (SI)'
}, {
    t('\\qty{'),
    d(1, insert_wrap), t('}{'),
    i(2), t('}')
}),

s({
    trig = 'ppt',
    name = 'short pmatrix OR short inline matrix',
    wordTrig = false,
    condition = mathzone,
    show_condition = mathzone
}, {
    c(1, {
        sn(nil, { t('\\p{'), d(1, insert_wrap), t('}') }),
        sn(nil, { t('\\pt{'), d(1, insert_wrap), t('}') })
    })
}),

s({
    trig = 'frac',
    name = 'fraction',
    wordTrig = false,
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\frac{'),
    d(1, insert_wrap), t('}{'),
    i(2), t('}')
}),

s({
    trig = '(%w)//(%w)',
    name = 'simple fraction',
    wordTrig = false,
    regTrig = true,
    priority = 1001,
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\frac{'),
    f(function(_, snip) return snip.captures[1] end), t('}{'),
    f(function(_, snip) return snip.captures[2] end), t('}')
}),

s({
    trig = 'sqrt',
    name = 'root',
    wordTrig = false,
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\sqrt'),
    n(2, '['), i(2), n(2, ']'),
    t('{'), d(1, insert_wrap), t('}')
}),


s({
    trig = 'bnom',
    name = 'binomial coefficient',
    wordTrig = false,
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\binom{'),
    i(1), t('}{'),
    i(2), t('}')
}),

s({
    trig = 'sum',
    name = 'sum',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\sum_'),
    m(1, wrap_braces, '{'), i(1), m(1, wrap_braces, '}')
}),

s({
    trig = 'prod',
    name = 'product',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\prod_'),
    m(1, wrap_braces, '{'), i(1), m(1, wrap_braces, '}')
}),

s({
    trig = 'coprod',
    name = 'coproduct',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\coprod_'),
    m(1, wrap_braces, '{'), i(1), m(1, wrap_braces, '}')
}),

s({
    trig = 'int',
    name = 'integral',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\int'), i(1),
    t(' \\dl{'), i(2), t('}')
}),

s({
    trig = 'ddx',
    name = 'differential',
    condition = mathzone,
    show_condition = mathzone
}, {
    c(1, {
        sn(nil, {
            t('\\diff'),
            n(3, '['), i(3), n(3, ']'),
            t('{'), i(1), t('}{'), i(2), t('}')
        }),
        sn(nil, {
            t('\\diff*'),
            n(3, '['), i(3), n(3, ']'),
            t('{'), i(1), t('}{'), i(2), t('}')
        })
    })
}),

s({
    trig = 'dpx',
    name = 'partial differential',
    condition = mathzone,
    show_condition = mathzone
}, {
    c(1, {
        sn(nil, {
            t('\\diffp'),
            n(3, '['), i(3), n(3, ']'),
            t('{'), i(1), t('}{'), i(2), t('}')
        }),
        sn(nil, {
            t('\\diffp*'),
            n(3, '['), i(3), n(3, ']'),
            t('{'), i(1), t('}{'), i(2), t('}')
        })
    })
}),

s({
    trig = 'dcp',
    name = 'compact partial differential',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\difcp'),
    n(3, '['), i(3), n(3, ']'),
    t('{'), i(1), t('}{'), i(2), t('}')
}),

s({
    trig = 'eval',
    name = 'integral/differential evaluation',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\eval{'),
    i(1), t('}{'),
    i(2), t('}')
}),

s({
    trig = 'lime',
    name = 'limit',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\lim_'),
    m(1, wrap_braces, '{'), i(1), m(1, wrap_braces, '}')
}),

s({
    trig = 'colim',
    name = 'colimit',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\colim_'),
    m(1, wrap_braces, '{'), i(1), m(1, wrap_braces, '}')
}),

s({
    trig = 'limi',
    name = 'limit inferior',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\liminf_{'),
    i(1), t(' \\to '),
    i(2), t('} ')
}),

s({
    trig = 'lims',
    name = 'limit superior',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\limsup_{'),
    i(1), t(' \\to '),
    i(2), t('} ')
}),

s({
    trig = 'ilim',
    name = 'inverse limit',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\varprojlim_'),
    m(1, wrap_braces, '{'), i(1), m(1, wrap_braces, '}')
}),

s({
    trig = 'dlim',
    name = 'direct limit',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\varinjlim_'),
    m(1, wrap_braces, '{'), i(1), m(1, wrap_braces, '}')
}),

s({
    trig = 'Nn',
    name = 'intersection',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\bigcap_'),
    m(1, wrap_braces, '{'), i(1), m(1, wrap_braces, '}')
}),

s({
    trig = 'Uu',
    name = 'union',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\bigcup_'),
    m(1, wrap_braces, '{'), i(1), m(1, wrap_braces, '}')
}),

s({
    trig = 'dUu',
    name = 'disjoint union',
    priority = 1001,
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\bigsqcup_'),
    m(1, wrap_braces, '{'), i(1), m(1, wrap_braces, '}')
}),

s({
    trig = 'Vv',
    name = 'wedge sum',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\bigvee_'),
    m(1, wrap_braces, '{'), i(1), m(1, wrap_braces, '}')
}),

s({
    trig = 'Aa',
    name = 'smash product',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\bigwedge_'),
    m(1, wrap_braces, '{'), i(1), m(1, wrap_braces, '}')
}),

s({
    trig = 'Xx',
    name = 'cross product',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\bigtimes_'),
    m(1, wrap_braces, '{'), i(1), m(1, wrap_braces, '}')
}),

s({
    trig = 'O++',
    name = 'direct sum',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\bigoplus_'),
    m(1, wrap_braces, '{'), i(1), m(1, wrap_braces, '}')
}),

s({
    trig = 'Oxx',
    name = 'direct product',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\bigotimes_'),
    m(1, wrap_braces, '{'), i(1), m(1, wrap_braces, '}')
}),

s({
    trig = 'sup',
    name = 'supremum',
    wordTrig = false,
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\sup')
}),

s({
    trig = 'inf',
    name = 'infimum',
    wordTrig = false,
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\inf')
}),

s({
    trig = 'max',
    name = 'maximum',
    wordTrig = false,
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\max')
}),

s({
    trig = 'min',
    name = 'minimum',
    wordTrig = false,
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\min'),
}),

s({
    trig = 'argmax',
    name = 'argument maximum',
    wordTrig = false,
    priority = 1001,
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\argmax_'),
    m(1, wrap_braces, '{'), i(1), m(1, wrap_braces, '}')
}),

s({
    trig = 'argmin',
    name = 'argument minimum',
    wordTrig = false,
    priority = 1001,
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\argmin_'),
    m(1, wrap_braces, '{'), i(1), m(1, wrap_braces, '}')
}),

-- Math strings ------------------------------------------------------------

s({
    trig = 'sin',
    name = 'sine',
    wordTrig = false,
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\sin')
}),

s({
    trig = 'cos',
    name = 'cosine',
    wordTrig = false,
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\cos')
}),

s({
    trig = 'tan',
    name = 'tangent',
    wordTrig = false,
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\tan')
}),

s({
    trig = 'arcsin',
    name = 'arcsine',
    wordTrig = false,
    priority = 1001,
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\arcsin')
}),

s({
    trig = 'arccos',
    name = 'arccosine',
    wordTrig = false,
    priority = 1001,
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\arccos')
}),

s({
    trig = 'arctan',
    name = 'arctangent',
    wordTrig = false,
    priority = 1001,
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\arctan')
}),

s({
    trig = 'log',
    name = 'logarithm',
    wordTrig = false,
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\log')
}),

s({
    trig = 'ln',
    name = 'natural logarithm',
    wordTrig = false,
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\ln')
}),

s({
    trig = 'exp',
    name = 'exponential',
    wordTrig = false,
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\exp')
}),

s({
    trig = 'id',
    name = 'identity',
    wordTrig = false,
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\id')
}),

s({
    trig = 'Pr',
    name = 'probability',
    wordTrig = false,
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\Pr')
}),

s({
    trig = 'grad',
    name = 'gradient',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\grad')
}),

s({
    trig = 'div',
    name = 'divergence',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\mdiv')
}),

s({
    trig = 'curl',
    name = 'curl',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\curl')
}),

s({
    trig = 'dim',
    name = 'dimension',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\dim')
}),

s({
    trig = 'ker',
    name = 'kernel',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\ker')
}),

s({
    trig = 'coker',
    name = 'cokernel',
    priority = 1001,
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\coker')
}),

s({
    trig = 'im',
    name = 'image',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\im')
}),

s({
    trig = 'hom',
    name = 'homomorphism',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\hom')
}),

s({
    trig = 'end',
    name = 'endomorphism',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\End')
}),

s({
    trig = 'aut',
    name = 'automorphism group',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\Aut')
}),

s({
    trig = 'ext',
    name = 'Ext group',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\Ext')
}),

s({
    trig = 'tor',
    name = 'Tor group',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\Tor')
}),

s({
    trig = 'quot',
    name = 'quotient field',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\Quot')
}),

s({
    trig = 'gal',
    name = 'galois group',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\Gal')
}),

s({
    trig = 'map',
    name = 'maps',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\map')
}),

s({
    trig = 'deg',
    name = 'degree',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\deg')
}),

s({
    trig = 'rk',
    name = 'rank',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\rk')
}),

s({
    trig = 'tr',
    name = 'trace',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\tr')
}),

s({
    trig = 'det',
    name = 'determinant',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\det')
}),

s({
    trig = 'span',
    name = 'span',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\mspan')
}),

s({
    trig = 'diag',
    name = 'diagonal matrix',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\diag')
}),

s({
    trig = 'spec',
    name = 'prime spectrum',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\Spec')
}),

s({
    trig = 'char',
    name = 'characteristic',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\Char')
}),

s({
    trig = 'Im',
    name = 'imaginary part',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\Im')
}),

s({
    trig = 'Re',
    name = 'real part',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\Re')
}),

s({
    trig = 'arg',
    name = 'complex argument',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\arg')
}),

s({
    trig = 'gcd',
    name = 'greatest common divisor',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\gcd')
}),

s({
    trig = 'lcm',
    name = 'least common multiple',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\lcm')
}),

s({
    trig = 'sgn',
    name = 'sign',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\sgn')
}),

s({
    trig = 'ord',
    name = 'order',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\ord')
}),

s({
    trig = 'mod',
    name = 'modulo',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\mod')
}),

-- Delimiters --------------------------------------------------------------{{{1

s({
    trig = 'abs',
    name = 'absolute value',
    wordTrig = false,
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\abs{'), d(1, insert_wrap), t('}')
}),

s({
    trig = 'Abs',
    name = 'absolute value (scalable delimiters',
    wordTrig = false,
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\abs*{'), d(1, insert_wrap), t('}')
}),

s({
    trig = 'norm',
    name = 'norm',
    wordTrig = false,
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\norm{'), d(1, insert_wrap), t('}')
}),

s({
    trig = 'Norm',
    name = 'norm (scalable delimiters',
    wordTrig = false,
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\norm*{'), d(1, insert_wrap), t('}')
}),

s({
    trig = 'dotp',
    name = 'dot product',
    wordTrig = false,
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\dotp{'), d(1, insert_wrap), t('}')
}),

s({
    trig = 'Dotp',
    name = 'dot product (scalable delimiters',
    wordTrig = false,
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\dotp*{'), d(1, insert_wrap), t('}')
}),

s({
    trig = 'floor',
    name = 'floor function',
    wordTrig = false,
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\floor{'), d(1, insert_wrap), t('}')
}),

s({
    trig = 'Floor',
    name = 'floor function (scalable delimiters',
    wordTrig = false,
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\floor*{'), d(1, insert_wrap), t('}')
}),

s({
    trig = 'ceil',
    name = 'ceil function',
    wordTrig = false,
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\ceil{'), d(1, insert_wrap), t('}')
}),

s({
    trig = 'Ceil',
    name = 'ceil function (scalable delimiters',
    wordTrig = false,
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\ceil*{'), d(1, insert_wrap), t('}')
}),

s({
    trig = 'mset',
    name = 'set',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\mset{'), d(1, insert_wrap), t('}')
}),

s({
    trig = 'Mset',
    name = 'set (scalable delimiters)',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\mset*{'), d(1, insert_wrap), t('}')
}),

s({
    trig = 'pn',
    name = 'scalable parantheses',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\pn*{'), d(1, insert_wrap), t('}')
}),

s({
    trig = 'dpn',
    name = 'double parantheses',
    priority = 1001,
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\dpn{'), d(1, insert_wrap), t('}')
}),

s({
    trig = 'bk',
    name = 'scalable brackets',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\bk*{'), d(1, insert_wrap), t('}')
}),

s({
    trig = 'dbk',
    name = 'double brackets',
    priority = 1001,
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\dbk{'), d(1, insert_wrap), t('}')
}),

-- Greek letters -----------------------------------------------------------{{{1

s({
    trig = 'alph',
    name = 'alpha',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\alpha')
}),

s({
    trig = 'bet',
    name = 'beta',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\beta')
}),

s({
    trig = 'gam',
    name = 'gamma',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\gamma')
}),

s({
    trig = 'Gam',
    name = 'Gamma',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\Gamma')
}),

s({
    trig = 'vGam',
    name = 'varGamma',
    priority = 1001,
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\varGamma')
}),

s({
    trig = 'del',
    name = 'delta',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\delta')
}),

s({
    trig = 'Del',
    name = 'Delta',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\Delta')
}),

s({
    trig = 'vDel',
    name = 'varDelta',
    priority = 1001,
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\varDelta')
}),

s({
    trig = 'eps',
    name = 'epsilon (varepsilon)',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\eps')
}),

s({
    trig = 'zet',
    name = 'zeta',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\zeta')
}),

s({
    trig = 'eta',
    name = 'eta',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\eta')
}),

s({
    trig = 'thet',
    name = 'theta',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\theta')
}),

s({
    trig = 'Thet',
    name = 'Theta',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\Theta')
}),

s({
    trig = 'vthet',
    name = 'vartheta',
    priority = 1001,
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\vartheta')
}),

s({
    trig = 'vThet',
    name = 'varTheta',
    priority = 1001,
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\varTheta')
}),

s({
    trig = 'iot',
    name = 'iota',
    priority = 1001,
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\iota')
}),

s({
    trig = 'kap',
    name = 'kappa',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\kappa')
}),

s({
    trig = 'vkap',
    name = 'varkappa',
    priority = 1001,
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\varkappa')
}),

s({
    trig = 'lam',
    name = 'lambda',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\lambda')
}),

s({
    trig = 'Lam',
    name = 'Lambda',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\Lambda')
}),

s({
    trig = 'vLam',
    name = 'varLambda',
    priority = 1001,
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\varLambda')
}),

s({
    trig = 'mu',
    name = 'mu',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\mu')
}),

s({
    trig = 'nu',
    name = 'nu',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\nu')
}),

s({
    trig = 'xi',
    name = 'xi',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\xi')
}),

s({
    trig = 'pi',
    name = 'pi',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\pi')
}),

s({
    trig = 'Pi',
    name = 'Pi',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\Pi')
}),

s({
    trig = 'vpi',
    name = 'varpi',
    priority = 1001,
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\varpi')
}),

s({
    trig = 'vPi',
    name = 'varPi',
    priority = 1001,
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\varPi')
}),

s({
    trig = 'rho',
    name = 'rho',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\rho')
}),

s({
    trig = 'sig',
    name = 'sig',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\sigma')
}),

s({
    trig = 'Sig',
    name = 'Sig',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\Sigma')
}),

s({
    trig = 'vSig',
    name = 'varSig',
    priority = 1001,
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\varSigma')
}),

s({
    trig = 'tau',
    name = 'tau',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\tau')
}),

s({
    trig = 'phi',
    name = 'phi',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\phi')
}),

s({
    trig = 'Phi',
    name = 'Phi',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\Phi')
}),

s({
    trig = 'vphi',
    name = 'varphi',
    priority = 1001,
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\varphi')
}),

s({
    trig = 'vPhi',
    name = 'varPhi',
    priority = 1001,
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\varPhi')
}),

s({
    trig = 'psi',
    name = 'psi',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\psi')
}),

s({
    trig = 'Psi',
    name = 'Psi',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\Psi')
}),

s({
    trig = 'vPsi',
    name = 'varPsi',
    priority = 1001,
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\varPsi')
}),

s({
    trig = 'chi',
    name = 'chi',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\chi')
}),

s({
    trig = 'ome',
    name = 'omega',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\omega')
}),

s({
    trig = 'Ome',
    name = 'Omega',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\Omega')
}),

s({
    trig = 'vOme',
    name = 'varOmega',
    priority = 1001,
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\varOmega')
}),

-- Math symbols ------------------------------------------------------------{{{1

s({
    trig = 'ii',
    name = 'imaginary unit',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\ii')
}),

s({
    trig = 'ee',
    name = 'Euler\'s constant',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\e')
}),

s({
    trig = 'NN',
    name = 'natural numbers',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\N')
}),

s({
    trig = 'ZZ',
    name = 'integers',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\Z')
}),

s({
    trig = 'QQ',
    name = 'rational numbers',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\Q')
}),

s({
    trig = 'RR',
    name = 'real numbers',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\R')
}),

s({
    trig = 'CC',
    name = 'complex numbers',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\C')
}),

s({
    trig = 'nab',
    name = 'nabla',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\nabla')
}),

s({
    trig = 'DD',
    name = 'differential operator',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\D')
}),

s({
    trig = 'oo',
    name = 'infinity',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\infty')
}),

s({
    trig = 'pow',
    name = 'powerset',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\powerset')
}),

s({
    trig = 'OO',
    name = 'empty set',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\varnothing')
}),

s({
    trig = '\\imp',
    name = 'implication',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\implies')
}),

s({
    trig = 'iff',
    name = 'equivalence',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\iff')
}),

s({
    trig = ':=',
    name = 'defined by',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\coloneqq')
}),

s({
    trig = '=:',
    name = 'defines',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\eqqcolon')
}),

s({
    trig = 'b:',
    name = 'binary colon',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\bcol')
}),

s({
    trig = '::',
    name = 'colon',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\colon')
}),

s({
    trig = '->',
    name = 'to',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\to')
}),

s({
    trig = '<-',
    name = 'gets',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\gets')
}),

s({
    trig = 'p->',
    name = 'parallel arrows',
    priority = 1001,
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\rightrightarrows')
}),

s({
    trig = 'a->',
    name = 'anti-parallel arrows',
    priority = 1001,
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\rightleftarrows')
}),

s({
    trig = 'x->',
    name = 'scalable arrow',
    priority = 1001,
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\xrightarrow'),
    n(2, '['), i(2), n(2, ']'),
    t('{'), d(1, insert_wrap), t('}')
}),

s({
    trig = '>-',
    name = 'maps to',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\mapsto')
}),

s({
    trig = '-<',
    name = 'maps from',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\mapsfrom')
}),

s({
    trig = '/>',
    name = 'converges increasingly',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\nearrow')
}),

s({
    trig = '\\>',
    name = 'converges decreasingly',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\searrow')
}),

s({
    trig = 'c->',
    name = 'injection',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\inj')
}),

s({
    trig = '\\to>', -- Use as '->>'
    name = 'surjection',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\proj')
}),

s({
    trig = '\\to~', -- Use as '->~'
    name = 'isomorphism',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\iso')
}),

s({
    trig = '=>',
    name = 'natural transformation',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\Rightarrow')
}),

s({
    trig = '..',
    name = 'automatic dots',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\dots')
}),

s({
    trig = 'c..',
    name = 'centered dots',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\cdots')
}),

s({
    trig = 'v..',
    name = 'vertical dots',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\vdots')
}),

s({
    trig = 'd..',
    name = 'diagonal dots',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\ddots')
}),

s({
    trig = 'bll',
    name = 'bullet',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\bullet')
}),

s({
    trig = 'prt',
    name = 'partial',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\partial')
}),

s({
    trig = 'EE',
    name = 'exists',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\exists')
}),

s({
    trig = '\\\\\\',
    name = 'right quotient',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\backslash')
}),

-- Binary operators --------------------------------------------------------{{{1

s({
    trig = 'inn',
    name = 'element of',
    priority = 1001, -- Clashes with 'nn'
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\in')
}),

s({
    trig = 'nin',
    name = 'not element of',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\notin')
}),

s({
    trig = 'nni',
    name = 'element from',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\ni')
}),

s({
    trig = 'cc',
    name = 'proper subset',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\subset')
}),

s({
    trig = 'ncc',
    name = 'not proper subset',
    priority = 1001,
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\nsubset')
}),

s({
    trig = 'Cc',
    name = 'subset',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\subseteq')
}),

s({
    trig = 'nCc',
    name = 'not subset',
    priority = 1001,
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\nsubseteq')
}),

s({
    trig = 'Cnc',
    name = 'proper subset',
    priority = 1001,
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\subsetneq')
}),

s({
    trig = 'sps',
    name = 'proper superset',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\supset')
}),

s({
    trig = 'nsps',
    name = 'not proper superset',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\nsupset')
}),

s({
    trig = 'Sps',
    name = 'superset',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\supseteq')
}),

s({
    trig = 'nSps',
    name = 'not superset',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\nsupseteq')
}),

s({
    trig = 'Spns',
    name = 'proper superset',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\supsetneq')
}),

s({
    trig = 'nn',
    name = 'intersection',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\cap')
}),

s({
    trig = 'uu',
    name = 'union',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\cup')
}),

s({
    trig = 'duu',
    name = 'disjoint union',
    priority = 1001,
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\sqcup')
}),

s({
    trig = 'ama',
    name = 'disjoint union (amalgamation)',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\amalg')
}),

s({
    trig = 'vv',
    name = 'wedge sum',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\vee')
}),

s({
    trig = 'aa',
    name = 'smash product',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\wedge')
}),

s({
    trig = '--',
    name = 'set difference',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\setminus')
}),

s({
    trig = '||',
    name = 'mid',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\mid')
}),

s({
    trig = 'n||',
    name = 'not mid',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\nmid')
}),

s({
    trig = 'm||',
    name = 'scalable mid',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\mmid')
}),

s({
    trig = '+-',
    name = 'plus/minus',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\pm')
}),

s({
    trig = '-+',
    name = 'minus/plus',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\mp')
}),

s({
    trig = '**',
    name = 'dot',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\cdot')
}),

s({
    trig = 'xx',
    name = 'times',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\times')
}),

s({
    trig = 'cmp',
    name = 'composition',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\circ')
}),

s({
    trig = 'perp',
    name = 'perpendicular',
    wordTrig = false,
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\perp')
}),

s({
    trig = 'o++',
    name = 'direct sum',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\oplus')
}),

s({
    trig = 'oxx',
    name = 'tensor product',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\otimes')
}),

s({
    trig = 'cup',
    name = 'cup product',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\smile')
}),

s({
    trig = 'cap',
    name = 'cap product',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\frown')
}),

-- Relations ---------------------------------------------------------------{{{1

s({
    trig = 'le',
    name = 'less equal',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\le')
}),

s({
    trig = 'ge',
    name = 'greater equal',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\ge')
}),

s({
    trig = 'ne',
    name = 'not equal',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\ne')
}),

s({
    trig = '<<',
    name = 'much less',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\ll')
}),

s({
    trig = '>>',
    name = 'much greater',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\gg')
}),

s({
    trig = 'sim',
    name = 'similar',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\sim')
}),

s({
    trig = 'iso',
    name = 'isomorphic',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\simeq')
}),

s({
    trig = 'cong',
    name = 'congruent',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\cong')
}),

s({
    trig = 'ncong',
    name = 'not congruent',
    priority = 1001,
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\ncong')
}),

s({
    trig = 'equiv',
    name = 'equivalent',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\equiv')
}),

s({
    trig = 'approx',
    name = 'approximately',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\approx')
}),

s({
    trig = 'ns',
    name = 'proper normal subgroup',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\vartriangleleft')
}),

s({
    trig = 'Ns',
    name = 'normal subgroup',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\trianglelefteq')
}),

-- Math environments -------------------------------------------------------{{{1

s({
    trig = 'meq',
    name = 'math equation OR enumerated math equation',
    condition = conds.line_begin,
    show_condition = conds.line_begin
}, {
    c(1, {
        sn(nil, {
            t({'\\begin{equation*}', '\t'}),
            i(1),
            t({'', '\\end{equation*}'})
        }),
        sn(nil, {
            t({'\\begin{equation}', '\t'}),
            i(1),
            t({'', '\\end{equation}'})
        })
    })
}),

s({
    trig = 'mal',
    name = 'math align OR enumerated math align',
    condition = conds.line_begin,
    show_condition = conds.line_begin
}, {
    c(1, {
        sn(nil, {
            t({'\\begin{align*}', '\t'}),
            i(1),
            t({'', '\\end{align*}'})
        }),
        sn(nil, {
            t({'\\begin{align}', '\t'}),
            i(1),
            t({'', '\\end{align}'})
        })
    })
}),

s({
    trig = 'mga',
    name = 'math gather OR enumerated math gather',
    condition = conds.line_begin,
    show_condition = conds.line_begin
}, {
    c(1, {
        sn(nil, {
            t({'\\begin{gather*}', '\t'}),
            i(1),
            t({'', '\\end{gather*}'})
        }),
        sn(nil, {
            t({'\\begin{gather}', '\t'}),
            i(1),
            t({'', '\\end{gather}'})
        })
    })
}),

s({
    trig = 'mmu',
    name = 'math multline OR enumerated math multline',
    condition = conds.line_begin,
    show_condition = conds.line_begin
}, {
    c(1, {
        sn(nil, {
            t({'\\begin{multline*}', '\t'}),
            i(1),
            t({'', '\\end{multline*}'})
        }),
        sn(nil, {
            t({'\\begin{multline}', '\t'}),
            i(1),
            t({'', '\\end{multline}'})
        })
    })
}),

s({
    trig = 'case',
    name = 'cases OR cases in displaystyle',
    condition = conds.line_begin,
    show_condition = conds.line_begin
}, {
    c(1, {
        sn(nil, {
            t({'\\begin{cases}', '\t'}),
            i(1),
            t({'', '\\end{cases}'})
        }),
        sn(nil, {
            t({'\\begin{dcases}', '\t'}),
            i(1),
            t({'', '\\end{dcases}'})
        })
    })
}),

s({
    trig = 'pmat',
    name = 'pmatrix OR bmatrix',
    condition = conds.line_begin,
    show_condition = conds.line_begin
}, {
    c(1, {
        sn(nil, {
            t({'\\begin{pmatrix}', '\t'}),
            i(1),
            t({'', '\\end{pmatrix}'})
        }),
        sn(nil, {
            t({'\\begin{bmatrix}', '\t'}),
            i(1),
            t({'', '\\end{bmatrix}'})
        })
    })
}),

-- Math theorem environments -----------------------------------------------{{{1

s({
    trig = 'thm',
    name = 'theorem environment',
    condition = conds.line_begin,
    show_condition = conds.line_begin
}, {
    c(1, {
        sn(nil, {
            t('\\begin{theorem}'), n(1, '['), i(1), n(1, ']'),
            t({'', '\t'}), i(2),
            t({'', '\\end{theorem}'})
        }),
        sn(nil, {
            t('\\begin{theorem}'), n(1, '['), i(1), n(1, ']'),
            t({'\\leavevmode', '\t\\begin{enumerate}', '\t\t\\item '}),
            i(2),
            t({'', '\t\\end{enumerate}', '\\end{theorem}'})
        }),
        sn(nil, {
            t('\\begin{theorem}'), n(1, '['), i(1), n(1, ']'),
            t({'\\leavevmode', '\t\\begin{itemize}', '\t\t\\item '}),
            i(2),
            t({'', '\t\\end{itemize}', '\\end{theorem}'})
        })
    })
}),

s({
    trig = 'prp',
    name = 'proposition environment',
    condition = conds.line_begin,
    show_condition = conds.line_begin
}, {
    c(1, {
        sn(nil, {
            t('\\begin{proposition}'), n(1, '['), i(1), n(1, ']'),
            t({'', '\t'}), i(2),
            t({'', '\\end{proposition}'})
        }),
        sn(nil, {
            t('\\begin{proposition}'), n(1, '['), i(1), n(1, ']'),
            t({'\\leavevmode', '\t\\begin{enumerate}', '\t\t\\item '}),
            i(2),
            t({'', '\t\\end{enumerate}', '\\end{proposition}'})
        }),
        sn(nil, {
            t('\\begin{proposition}'), n(1, '['), i(1), n(1, ']'),
            t({'\\leavevmode', '\t\\begin{itemize}', '\t\t\\item '}),
            i(2),
            t({'', '\t\\end{itemize}', '\\end{proposition}'})
        })
    })
}),

s({
    trig = 'lmm',
    name = 'lemma environment',
    condition = conds.line_begin,
    show_condition = conds.line_begin
}, {
    c(1, {
        sn(nil, {
            t('\\begin{lemma}'), n(1, '['), i(1), n(1, ']'),
            t({'', '\t'}), i(2),
            t({'', '\\end{lemma}'})
        }),
        sn(nil, {
            t('\\begin{lemma}'), n(1, '['), i(1), n(1, ']'),
            t({'\\leavevmode', '\t\\begin{enumerate}', '\t\t\\item '}),
            i(2),
            t({'', '\t\\end{enumerate}', '\\end{lemma}'})
        }),
        sn(nil, {
            t('\\begin{lemma}'), n(1, '['), i(1), n(1, ']'),
            t({'\\leavevmode', '\t\\begin{itemize}', '\t\t\\item '}),
            i(2),
            t({'', '\t\\end{itemize}', '\\end{lemma}'})
        })
    })
}),

s({
    trig = 'coy',
    name = 'corollary environment',
    condition = conds.line_begin,
    show_condition = conds.line_begin
}, {
    c(1, {
        sn(nil, {
            t('\\begin{corollary}'), n(1, '['), i(1), n(1, ']'),
            t({'', '\t'}), i(2),
            t({'', '\\end{corollary}'})
        }),
        sn(nil, {
            t('\\begin{corollary}'), n(1, '['), i(1), n(1, ']'),
            t({'\\leavevmode', '\t\\begin{enumerate}', '\t\t\\item '}),
            i(2),
            t({'', '\t\\end{enumerate}', '\\end{corollary}'})
        }),
        sn(nil, {
            t('\\begin{corollary}'), n(1, '['), i(1), n(1, ']'),
            t({'\\leavevmode', '\t\\begin{itemize}', '\t\t\\item '}),
            i(2),
            t({'', '\t\\end{itemize}', '\\end{corollary}'})
        })
    })
}),

s({
    trig = 'fac',
    name = 'fact environment',
    condition = conds.line_begin,
    show_condition = conds.line_begin
}, {
    c(1, {
        sn(nil, {
            t('\\begin{fact}'), n(1, '['), i(1), n(1, ']'),
            t({'', '\t'}), i(2),
            t({'', '\\end{fact}'})
        }),
        sn(nil, {
            t('\\begin{fact}'), n(1, '['), i(1), n(1, ']'),
            t({'\\leavevmode', '\t\\begin{enumerate}', '\t\t\\item '}),
            i(2),
            t({'', '\t\\end{enumerate}', '\\end{fact}'})
        }),
        sn(nil, {
            t('\\begin{fact}'), n(1, '['), i(1), n(1, ']'),
            t({'\\leavevmode', '\t\\begin{itemize}', '\t\t\\item '}),
            i(2),
            t({'', '\t\\end{itemize}', '\\end{fact}'})
        })
    })
}),

s({
    trig = 'dfn',
    name = 'definition environment',
    condition = conds.line_begin,
    show_condition = conds.line_begin
}, {
    c(1, {
        sn(nil, {
            t('\\begin{definition}'), n(1, '['), i(1), n(1, ']'),
            t({'', '\t'}), i(2),
            t({'', '\\end{definition}'})
        }),
        sn(nil, {
            t('\\begin{definition}'), n(1, '['), i(1), n(1, ']'),
            t({'\\leavevmode', '\t\\begin{enumerate}', '\t\t\\item '}),
            i(2),
            t({'', '\t\\end{enumerate}', '\\end{definition}'})
        }),
        sn(nil, {
            t('\\begin{definition}'), n(1, '['), i(1), n(1, ']'),
            t({'\\leavevmode', '\t\\begin{itemize}', '\t\t\\item '}),
            i(2),
            t({'', '\t\\end{itemize}', '\\end{definition}'})
        })
    })
}),

s({
    trig = 'ntn',
    name = 'notation environment',
    condition = conds.line_begin,
    show_condition = conds.line_begin
}, {
    c(1, {
        sn(nil, {
            t('\\begin{notation}'), n(1, '['), i(1), n(1, ']'),
            t({'', '\t'}), i(2),
            t({'', '\\end{notation}'})
        }),
        sn(nil, {
            t('\\begin{notation}'), n(1, '['), i(1), n(1, ']'),
            t({'\\leavevmode', '\t\\begin{enumerate}', '\t\t\\item '}),
            i(2),
            t({'', '\t\\end{enumerate}', '\\end{notation}'})
        }),
        sn(nil, {
            t('\\begin{notation}'), n(1, '['), i(1), n(1, ']'),
            t({'\\leavevmode', '\t\\begin{itemize}', '\t\t\\item '}),
            i(2),
            t({'', '\t\\end{itemize}', '\\end{notation}'})
        })
    })
}),

s({
    trig = 'rmk',
    name = 'remark environment',
    condition = conds.line_begin,
    show_condition = conds.line_begin
}, {
    c(1, {
        sn(nil, {
            t('\\begin{remark}'), n(1, '['), i(1), n(1, ']'),
            t({'', '\t'}), i(2),
            t({'', '\\end{remark}'})
        }),
        sn(nil, {
            t('\\begin{remark}'), n(1, '['), i(1), n(1, ']'),
            t({'\\leavevmode', '\t\\begin{enumerate}', '\t\t\\item '}),
            i(2),
            t({'', '\t\\end{enumerate}', '\\end{remark}'})
        }),
        sn(nil, {
            t('\\begin{remark}'), n(1, '['), i(1), n(1, ']'),
            t({'\\leavevmode', '\t\\begin{itemize}', '\t\t\\item '}),
            i(2),
            t({'', '\t\\end{itemize}', '\\end{remark}'})
        })
    })
}),

s({
    trig = 'wrn',
    name = 'warning environment',
    condition = conds.line_begin,
    show_condition = conds.line_begin
}, {
    c(1, {
        sn(nil, {
            t('\\begin{warning}'), n(1, '['), i(1), n(1, ']'),
            t({'', '\t'}), i(2),
            t({'', '\\end{warning}'})
        }),
        sn(nil, {
            t('\\begin{warning}'), n(1, '['), i(1), n(1, ']'),
            t({'\\leavevmode', '\t\\begin{enumerate}', '\t\t\\item '}),
            i(2),
            t({'', '\t\\end{enumerate}', '\\end{warning}'})
        }),
        sn(nil, {
            t('\\begin{warning}'), n(1, '['), i(1), n(1, ']'),
            t({'\\leavevmode', '\t\\begin{itemize}', '\t\t\\item '}),
            i(2),
            t({'', '\t\\end{itemize}', '\\end{warning}'})
        })
    })
}),

s({
    trig = 'axm',
    name = 'axiom environment',
    condition = conds.line_begin,
    show_condition = conds.line_begin
}, {
    c(1, {
        sn(nil, {
            t('\\begin{axiom}'), n(1, '['), i(1), n(1, ']'),
            t({'', '\t'}), i(2),
            t({'', '\\end{axiom}'})
        }),
        sn(nil, {
            t('\\begin{axiom}'), n(1, '['), i(1), n(1, ']'),
            t({'\\leavevmode', '\t\\begin{enumerate}', '\t\t\\item '}),
            i(2),
            t({'', '\t\\end{enumerate}', '\\end{axiom}'})
        }),
        sn(nil, {
            t('\\begin{axiom}'), n(1, '['), i(1), n(1, ']'),
            t({'\\leavevmode', '\t\\begin{itemize}', '\t\t\\item '}),
            i(2),
            t({'', '\t\\end{itemize}', '\\end{axiom}'})
        })
    })
}),

s({
    trig = 'obs',
    name = 'observation environment',
    condition = conds.line_begin,
    show_condition = conds.line_begin
}, {
    c(1, {
        sn(nil, {
            t('\\begin{observation}'), n(1, '['), i(1), n(1, ']'),
            t({'', '\t'}), i(2),
            t({'', '\\end{observation}'})
        }),
        sn(nil, {
            t('\\begin{observation}'), n(1, '['), i(1), n(1, ']'),
            t({'\\leavevmode', '\t\\begin{enumerate}', '\t\t\\item '}),
            i(2),
            t({'', '\t\\end{enumerate}', '\\end{observation}'})
        }),
        sn(nil, {
            t('\\begin{observation}'), n(1, '['), i(1), n(1, ']'),
            t({'\\leavevmode', '\t\\begin{itemize}', '\t\t\\item '}),
            i(2),
            t({'', '\t\\end{itemize}', '\\end{observation}'})
        })
    })
}),

s({
    trig = 'xrc',
    name = 'exercise environment',
    condition = conds.line_begin,
    show_condition = conds.line_begin
}, {
    c(1, {
        sn(nil, {
            t('\\begin{exercise}'), n(1, '['), i(1), n(1, ']'),
            t({'', '\t'}), i(2),
            t({'', '\\end{exercise}'})
        }),
        sn(nil, {
            t('\\begin{exercise}'), n(1, '['), i(1), n(1, ']'),
            t({'\\leavevmode', '\t\\begin{enumerate}', '\t\t\\item '}),
            i(2),
            t({'', '\t\\end{enumerate}', '\\end{exercise}'})
        }),
        sn(nil, {
            t('\\begin{exercise}'), n(1, '['), i(1), n(1, ']'),
            t({'\\leavevmode', '\t\\begin{itemize}', '\t\t\\item '}),
            i(2),
            t({'', '\t\\end{itemize}', '\\end{exercise}'})
        })
    })
}),

s({
    trig = 'xpl',
    name = 'example environment',
    condition = conds.line_begin,
    show_condition = conds.line_begin
}, {
    c(1, {
        sn(nil, {
            t('\\begin{example}'), n(1, '['), i(1), n(1, ']'),
            t({'', '\t'}), i(2),
            t({'', '\\end{example}'})
        }),
        sn(nil, {
            t('\\begin{example}'), n(1, '['), i(1), n(1, ']'),
            t({'\\leavevmode', '\t\\begin{enumerate}', '\t\t\\item '}),
            i(2),
            t({'', '\t\\end{enumerate}', '\\end{example}'})
        }),
        sn(nil, {
            t('\\begin{example}'), n(1, '['), i(1), n(1, ']'),
            t({'\\leavevmode', '\t\\begin{itemize}', '\t\t\\item '}),
            i(2),
            t({'', '\t\\end{itemize}', '\\end{example}'})
        })
    })
}),

s({
    trig = 'prb',
    name = 'problem environment',
    condition = conds.line_begin,
    show_condition = conds.line_begin
}, {
    c(1, {
        sn(nil, {
            t('\\begin{problem}'), n(1, '['), i(1), n(1, ']'),
            t({'', '\t'}), i(2),
            t({'', '\\end{problem}'})
        }),
        sn(nil, {
            t('\\begin{problem}'), n(1, '['), i(1), n(1, ']'),
            t({'\\leavevmode', '\t\\begin{enumerate}', '\t\t\\item '}),
            i(2),
            t({'', '\t\\end{enumerate}', '\\end{problem}'})
        }),
        sn(nil, {
            t('\\begin{problem}'), n(1, '['), i(1), n(1, ']'),
            t({'\\leavevmode', '\t\\begin{itemize}', '\t\t\\item '}),
            i(2),
            t({'', '\t\\end{itemize}', '\\end{problem}'})
        })
    })
}),

s({
    trig = 'sln',
    name = 'solution environment',
    condition = conds.line_begin,
    show_condition = conds.line_begin
}, {
    c(1, {
        sn(nil, {
            t('\\begin{solution}'), n(1, '['), i(1), n(1, ']'),
            t({'', '\t'}), i(2),
            t({'', '\\end{solution}'})
        }),
        sn(nil, {
            t('\\begin{solution}'), n(1, '['), i(1), n(1, ']'),
            t({'\\leavevmode', '\t\\begin{enumerate}', '\t\t\\item '}),
            i(2),
            t({'', '\t\\end{enumerate}', '\\end{solution}'})
        }),
        sn(nil, {
            t('\\begin{solution}'), n(1, '['), i(1), n(1, ']'),
            t({'\\leavevmode', '\t\\begin{itemize}', '\t\t\\item '}),
            i(2),
            t({'', '\t\\end{itemize}', '\\end{solution}'})
        })
    })
}),

s({
    trig = 'hgh', -- Necessary optional argument
    name = 'highlight environment',
    condition = conds.line_begin,
    show_condition = conds.line_begin
}, {
    c(1, {
        sn(nil, {
            t('\\begin{highlight}'), t('['), i(1), t(']'),
            t({'', '\t'}), i(2),
            t({'', '\\end{highlight}'})
        }),
        sn(nil, {
            t('\\begin{highlight}'), t('['), i(1), t(']'),
            t({'\\leavevmode', '\t\\begin{enumerate}', '\t\t\\item '}),
            i(2),
            t({'', '\t\\end{enumerate}', '\\end{highlight}'})
        }),
        sn(nil, {
            t('\\begin{highlight}'), t('['), i(1), t(']'),
            t({'\\leavevmode', '\t\\begin{itemize}', '\t\t\\item '}),
            i(2),
            t({'', '\t\\end{itemize}', '\\end{highlight}'})
        })
    })
}),

s({
    trig = 'prf',
    name = 'proof environment',
    condition = conds.line_begin,
    show_condition = conds.line_begin
}, {
    c(1, {
        sn(nil, {
            t('\\begin{proof}'), n(1, '['), i(1), n(1, ']'),
            t({'', '\t'}), i(2),
            t({'', '\\end{proof}'})
        }),
        sn(nil, {
            t('\\begin{proof}'), n(1, '['), i(1), n(1, ']'),
            t({'\\leavevmode', '\t\\begin{enumerate}', '\t\t\\item '}),
            i(2),
            t({'', '\t\\end{enumerate}', '\\end{proof}'})
        }),
        sn(nil, {
            t('\\begin{proof}'), n(1, '['), i(1), n(1, ']'),
            t({'\\leavevmode', '\t\\begin{itemize}', '\t\t\\item '}),
            i(2),
            t({'', '\t\\end{itemize}', '\\end{proof}'})
        })
    })
}),

-- Tikz-cd -----------------------------------------------------------------{{{1

s({
    trig = 'tcd',
    name = 'tikzcd environment',
    condition = mathzone,
    show_condition = mathzone 
}, {
    t('\\begin{tikzcd}'),
    t({'', '\t'}), i(1),
    t({'', '\\end{tikzcd}'})
}),

s({
    trig = 'tar',
    name = 'tikzcd arrow',
    condition = tikzcd,
    show_condition = tikzcd 
}, {
    t('\\ar['), i(1), t(']')
}),

s({
    trig = 'tlb',
    name = 'tikzcd label',
    condition = tikzcd,
    show_condition = tikzcd 
}, {
    t('"'), i(1), t('"')
}),

s({
    trig = 'tbl',
    name = 'tikzcd bend left',
    condition = tikzcd,
    show_condition = tikzcd 
}, {
    t('bend left')
}),

s({
    trig = 'tbr',
    name = 'tikzcd bend right',
    condition = tikzcd,
    show_condition = tikzcd 
}, {
    t('bend right')
}),

s({
    trig = 'tsl',
    name = 'tikzcd shift left',
    condition = tikzcd,
    show_condition = tikzcd 
}, {
    t('shift left')
}),

s({
    trig = 'tsr',
    name = 'tikzcd shift right',
    condition = tikzcd,
    show_condition = tikzcd 
}, {
    t('shift right')
}),

s({
    trig = 'tll',
    name = 'tikzcd loop left',
    condition = tikzcd,
    show_condition = tikzcd 
}, {
    t('loop left')
}),

s({
    trig = 'tlr',
    name = 'tikzcd loop right',
    condition = tikzcd,
    show_condition = tikzcd 
}, {
    t('loop right')
}),

s({
    trig = 'tdh',
    name = 'tikzcd dash',
    condition = tikzcd,
    show_condition = tikzcd 
}, {
    t('dash')
}),

s({
    trig = 'tdd',
    name = 'tikzcd dashed',
    condition = tikzcd,
    show_condition = tikzcd 
}, {
    t('dashed')
}),

s({
    trig = 'teq',
    name = 'tikzcd equals',
    condition = tikzcd,
    show_condition = tikzcd 
}, {
    t('equals')
}),

s({
    trig = 'tmt',
    name = 'tikzcd mapsto',
    condition = tikzcd,
    show_condition = tikzcd 
}, {
    t('mapsto')
}),

s({
    trig = 'tij',
    name = 'tikzcd injective morphism',
    condition = tikzcd,
    show_condition = tikzcd 
}, {
    t('hook')
}),

s({
    trig = 'tsj',
    name = 'tikzcd surjective morphism',
    condition = tikzcd,
    show_condition = tikzcd 
}, {
    t('two heads')
}),

s({
    trig = 'tbj',
    name = 'tikzcd bijective morphism',
    condition = tikzcd,
    show_condition = tikzcd 
}, {
    t('"\\sim" sloped')
}),

s({
    trig = 'tBj',
    name = 'tikzcd bijective morphism, label other side',
    condition = tikzcd,
    show_condition = tikzcd 
}, {
    t('"\\sim"\' sloped')
}),

s({
    trig = 'tpush',
    name = 'tikzcd pushout',
    condition = tikzcd,
    show_condition = tikzcd 
}, {
    i(1), t(' \\ar[r'), i(2), t('] \\ar[d'), i(3), t(']'),
    t({'', '& '}), i(4), t(' \\ar[d'), i(5), t({'] \\\\', ''}),
    i(6), t(' \\ar[r'), i(7), t(']'),
    t({'', '& '}), i(8)
}),

s({
    trig = 'tss',
    name = 'tikzcd triangle south',
    condition = tikzcd,
    show_condition = tikzcd 
}, {
    i(1), t(' \\ar[rr'), i(2), t(']'), i(3),
    t({'', '&& '}), i(4), t({' \\\\'}),
    t({'', '& '}), i(5)
}),

s({
    trig = 'tne',
    name = 'tikzcd triangle north east',
    condition = tikzcd,
    show_condition = tikzcd 
}, {
    i(1), t(' \\ar[r'), i(2), t(']'), i(3),
    t({'', '& '}), i(4), t({' \\\\'}),
    t({'', '& '}), i(5)
}),

s({
    trig = 'tnw',
    name = 'tikzcd triangle north west',
    condition = tikzcd,
    show_condition = tikzcd 
}, {
    i(1), t(' \\ar[r'), i(2), t(']'), i(3),
    t({'', '& '}), i(4), t({' \\\\', ''}),
    i(5)
}),

s({
    trig = 'tse',
    name = 'tikzcd triangle south east',
    condition = tikzcd,
    show_condition = tikzcd 
}, {
    t({'& '}), i(1), t({' \\\\', ''}),
    i(2), t(' \\ar[r'), i(3), t(']'), i(4),
    t({'', '& '}), i(5)
}),

s({
    trig = 'tsw',
    name = 'tikzcd triangle south west',
    condition = tikzcd,
    show_condition = tikzcd 
}, {
    i(1), t({' \\\\', ''}),
    i(2), t(' \\ar[r'), i(3), t(']'), i(4),
    t({'', '& '}), i(5)
}),

s({
    trig = 'tshort',
    name = 'tikzcd short exact sequence',
    condition = tikzcd,
    show_condition = tikzcd 
}, {
    i(1), t(' \\ar[r'), i(2), t(']'),
    t({'', '& '}), i(3), t(' \\ar[r'), i(4), t(']'),
    t({'', '& '}), i(5), t(' \\ar[r'), i(6), t(']'),
    t({'', '& '}), i(7), t(' \\ar[r'), i(8), t(']'),
    t({'', '& '}), i(9)
}),

s({
    trig = 'tlong',
    name = 'tikzcd term of long exact sequence',
    condition = tikzcd,
    show_condition = tikzcd 
}, {
    t({'& '}), i(1), t(' \\ar[r'), i(2), t(']')
}),

s({
    trig = 'tfive',
    name = 'tikzcd five lemma',
    condition = tikzcd,
    show_condition = tikzcd 
}, {
    i(1), t(' \\ar[r'), i(2), t('] \\ar[d'), i(19), t(']'),
    t({'', '& '}), i(3), t(' \\ar[r'), i(4), t('] \\ar[d'), i(20), t(']'),
    t({'', '& '}), i(5), t(' \\ar[r'), i(6), t('] \\ar[d'), i(21), t(']'),
    t({'', '& '}), i(7), t(' \\ar[r'), i(8), t('] \\ar[d'), i(22), t(']'),
    t({'', '& '}), i(9), t(' \\ar[d'), i(23), t({'] \\\\', ''}),
    i(10), t(' \\ar[r'), i(11), t(']'),
    t({'', '& '}), i(12), t(' \\ar[r'), i(13), t(']'),
    t({'', '& '}), i(14), t(' \\ar[r'), i(15), t(']'),
    t({'', '& '}), i(16), t(' \\ar[r'), i(17), t(']'),
    t({'', '& '}), i(18)
}),

s({
    trig = 'tpar',
    name = 'tikzcd parallel morphisms',
    condition = tikzcd,
    show_condition = tikzcd 
}, {
    t(' \\ar[r, shift left'), i(1), t(']'),
    t(' \\ar[r, shift right'), i(2), t(']')
}),

-- Misc --------------------------------------------------------------------{{{1

s({
    trig = 'docl',
    name = 'lecture document',
    condition = conds.line_begin,
    show_condition = conds.line_begin
}, fmta([[
    \documentclass{article}

    \usepackage[
        lecture,
        course={<>},
        code={<>},
        lecturer={<>},
        term={<>}
    ]{tienuni}

    \begin{document}

    <>
    
    \end{document}]],
    { i(1), i(2), i(3), i(4), i(0) }
)),

s({
    trig = 'tbib',
    name = 'manual bibliogrphy',
    condition = conds.line_begin,
    show_condition = conds.line_begin
}, {
    t('\\begin{thebibliography}{'), i(1), t({'}', '\t'}),
    i(0),
    t({'', '\\end{thebibliography}'})
}),

s({
    trig = 'pkg',
    name = 'usepackage',
    condition = conds.line_begin,
    show_condition = conds.line_begin
}, {
    t('\\usepackage'),
    n(2, '['), i(2), n(2, ']'),
    t('{'), i(1), t('}')
}),

s({
    trig = 'lab',
    name = 'label',
    condition = conds.line_begin,
    show_condition = conds.line_begin
}, {
    t('\\label{'), i(1), t('}')
}),

s({
    trig = 'rref',
    name = 'plain reference'
}, {
    t('\\ref{'), i(1), t('}')
}),

s({
    trig = 'cref',
    name = 'reference with text'
}, {
    t('\\cref{'), i(1), t('}')
}),

s({
    trig = 'Cref',
    name = 'reference with text (capitalised)'
}, {
    t('\\Cref{'), i(1), t('}')
}),

s({
    trig = 'lcref',
    name = 'reference without text'
}, {
    t('\\labelcref{'), i(1), t('}')
}),

s({
    trig = 'tps',
    name = 'tex or pdf string'
}, {
    t('\\texorpdfstring{'), d(1, insert_wrap), t('}'),
    t('{'), i(2), t('}')
}),

s({
    trig = 'enq',
    name = 'enquote',
}, {
    t('\\enquote{'), d(1, insert_wrap), t('}')
}),

s({
    trig = 'lvm',
    name = 'leavevmode',
}, {
    t('\\leavevmode')
}),

s({
    trig = 'quad',
    name = 'quad',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\quad')
}),

s({
    trig = 'qquad',
    name = 'qquad',
    priority = 1001,
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\qquad')
}),

s({
    trig = 'qed',
    name = 'qed'
}, {
    t('\\qedhere')
}),

s({
    trig = 'os',
    name = 'overset over relation',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\overset{'),
    i(2), t('}{'),
    d(1, insert_wrap), t('}')
}),

s({
    trig = 'stk',
    name = 'substack for subscripts',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\substack{'), d(1, insert_wrap), t('}')
}),

s({
    trig = 'tt',
    name = 'text in math',
    condition = mathzone,
    show_condition = mathzone
}, {
    t('\\text{'), d(1, insert_wrap), t('}')
}),

-- English text ------------------------------------------------------------{{{1

s({
    trig = 'ie',
    name = 'i.e.'
}, {
    t('i.\\,e.\\')
}),

s({
    trig = 'eg',
    name = 'e.g.'
}, {
    t('e.\\,g.\\')
}),

s({
    trig = 'Eg',
    name = 'E.g.'
}, {
    t('E.\\,g.\\')
}),

s({
    trig = 'resp',
    name = 'resp.'
}, {
    t('resp.\\')
}),

s({
    trig = 'wrt',
    name = 'w.r.t.'
}, {
    t('w.\\,r.\\,t.\\')
}),

s({
    trig = 'Wrt',
    name = 'W.r.t.'
}, {
    t('W.\\,r.\\,t.\\')
}),

s({
    trig = 'wlog',
    name = 'w.l.o.g.'
}, {
    t('w.\\,l.\\,o.\\,g.\\')
}),

s({
    trig = 'Wlog',
    name = 'W.l.o.g.'
}, {
    t('W.\\,l.\\,o.\\,g.\\')
}),

s({
    trig = '~~',
    wordTrig = false,
    name = 'non-breaking hyphen'
}, {
    t('\\nbhyph ')
}),

s({
    trig = '~+',
    wordTrig = false,
    name = 'breaking hyphen'
}, {
    t('\\bhyph ')
}),

-- German text -------------------------------------------------------------{{{1

s({
    trig = 'dh',
    name = 'd.h.'
}, {
    t('d.\\,h.\\')
}),

s({
    trig = 'Dh',
    name = 'D.h.'
}, {
    t('D.\\,h.\\')
}),

s({
    trig = 'zb',
    name = 'z.B.'
}, {
    t('z.\\,B.\\')
}),

s({
    trig = 'Zb',
    name = 'Z.B.'
}, {
    t('Z.\\,B.\\')
}),

s({
    trig = 'bspw',
    name = 'bspw.'
}, {
    t('bspw.\\')
}),

s({
    trig = 'Bspw',
    name = 'Bspw.'
}, {
    t('Bspw.\\')
}),

s({
    trig = 'bzw',
    name = 'bzw.'
}, {
    t('bzw.\\')
}),

s({
    trig = 'Bzw',
    name = 'Bzw.'
}, {
    t('Bzw.\\')
}),

s({
    trig = 'bzgl',
    name = 'bzgl.'
}, {
    t('bzgl.\\')
}),

s({
    trig = 'Bzgl',
    name = 'Bzgl.'
}, {
    t('Bzgl.\\')
}),

s({
    trig = 'etc',
    name = 'etc.'
}, {
    t('etc.\\')
}),

s({
    trig = 'usw',
    name = 'usw.'
}, {
    t('usw.\\')
}),

s({
    trig = 'obda',
    name = 'o.B.d.A.'
}, {
    t('o.\\,B.\\,d.\\,A.\\')
}),

s({
    trig = 'Obda',
    name = 'O.B.d.A.'
}, {
    t('O.\\,B.\\,d.\\,A.\\')
}),

}
