local vo = vim.opt

vo.cpoptions = '' -- No Vi compatibility

vo.ignorecase = true -- Ignore case while searching
vo.smartcase = true -- Respect case if there is one capital letter

vo.number = true -- Current line has absolute number
vo.relativenumber = true -- Other lines have relative number

vo.linebreak = true -- Soft wrap at white space
vo.breakindent = true -- Preserve indent of soft wrapped lines

vo.shiftwidth = 4 -- Indent line by 4 spaces
vo.tabstop = 4 -- Display tab as 4 spaces
vo.expandtab = true -- Tabs are spaces

vo.colorcolumn = '81' -- Visual vertical line for line length
vo.cursorline = true -- Highlight current line
vo.scrolloff = 4 -- Minimum distance from top/bottom while scrolling

vo.showmatch = true -- Highlight matching brackets
vo.matchtime = 2 -- Highlight for 200ms

vo.splitright = true -- ':vsplit' to the right
vo.splitbelow = true -- ':split' to the right

-- List all completion options, expand up to longest common substing
vo.wildmode = 'list:longest'
vo.wildignore = { -- Ignore files when completing files
    '*.odt', '*.ods', '*.odp',
    '*.doc', '*.docx', '*.ppt', '*.pptx', '*.xls', '*.xlsx',
    '*.pdf', '*.epub',
    '*.png', '*.jpg', '*.bmp',
    '*.zip', '*.tar', '*.rar', '*.gz',
    '*.out', '*.exe',
    '*.pyc',
    '*.o', '*.obj',
    '*.aux', '*.fdb_latexmk', '*.fls', '*.idx', '*.ilg', '*.ind', '*.toc'
}
vo.suffixes = { '.info', '.log' } -- Files with lower priority
vo.completeopt:append('noinsert') -- No insertion while selecting

vo.conceallevel = 2 -- Essentially for Vimtex

vo.foldmethod = 'marker' -- Enable folds

vo.spell = true -- Enable spell checking
vo.spelllang = { 'de_20', 'en_gb' } -- Languages to spell check
vo.spellfile = vim.fn.stdpath('config') ..
    '/spell/tien.utf-8.add' -- Custom correct words
