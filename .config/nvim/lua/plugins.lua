return require('packer').startup(function(use)
    use 'wbthomason/packer.nvim' -- Plugin manager

    use {'nvim-treesitter/nvim-treesitter', -- Parser for highlighting
        run = ':TSUpdate' -- Also update all parsers if updating
    }
    use 'L3MON4D3/LuaSnip' -- Snippet engine
    use 'neovim/nvim-lspconfig' -- Integrated LSP support

    use {'nvim-lualine/lualine.nvim', -- Status line
        requires = { 'nvim-tree/nvim-web-devicons' }
    }
    use 'Mofiqul/dracula.nvim' -- Dracula colorscheme
    use {'uloco/bluloco.nvim', -- Bluloco colorscheme
        requires = { 'rktjmp/lush.nvim' }
    }

    use 'lervag/vimtex'
end)
