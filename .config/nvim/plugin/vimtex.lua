-- Vimtex is THE BEST plug-in for Latex, which is also syntactically quite
-- different from other programming and markup languages. Superior are the 
-- commands 'tse', 'cse', the improved completion for omni-func and the
-- functions 'vimtex#syntax#in_mathzone()' and the like. I'm still searching for
-- an alternative way in pure Lua.
local g = vim.g

-- Shell escape for TikZ 'externalize'
g.vimtex_compiler_latexmk = {
    options = {
        '-shell-escape',
        '-verbose',
        '-file-line-error',
        '-synctex=1',
        '-interaction=nonstopmode',
        -- Disable latexmk's infinite loop due to change of time stamp
        "-e '$hash_calc_ignore_pattern{q/pdf/} = q/(CreationDate|ModDate|ID) /;'",
    }
}
if vim.loop.os_uname().sysname == 'Linux' then
    g.vimtex_view_method = 'zathura' -- View PDF with zathura
end

-- Also indent on brackets
g.vimtex_indent_enabled = false
--g.vimtex_indent_delims = {
--    open = { '{', '[' },
--    close = { '{', '[' }
--}
--g.vimtex_indent_on_ampersands = false

g.vimtex_fold_enabled = true -- Folding
g.vimtex_fold_types = {
    envs           = { enabled = false },
    envs_options   = { enabled = false },
    items          = { enabled = false },
    cmd_single     = { enabled = false },
    cmd_single_opt = { enabled = false },
    cmd_multi      = { enabled = false }
}
--g.vimtex_fold_manual = true

-- Concealing
g.vimtex_syntax_conceal = { -- Different from default settings
    spacing = false,
    math_fracs = false,
    math_super_sub = false,
    sections = true
}
g.vimtex_syntax_custom_cmds = { -- Custom replacements and concealments
    { name = 'N',           mathmode = true, concealchar = 'ℕ'},
    { name = 'Z',           mathmode = true, concealchar = 'ℤ'},
    { name = 'Q',           mathmode = true, concealchar = 'ℚ'},
    { name = 'R',           mathmode = true, concealchar = 'ℝ'},
    { name = 'C',           mathmode = true, concealchar = 'ℂ'},
    { name = 'F',           mathmode = true, concealchar = '𝔽'},
    { name = 'K',           mathmode = true, concealchar = '𝕂'},
    { name = 'h',           mathmode = true, concealchar = 'ℋ'},
    { name = 'P',           mathmode = true, concealchar = '𝔓'},
    { name = 'm',           mathmode = true, concealchar = '𝔪'},
    { name = 'p',           mathmode = true, concealchar = '𝔭'},
    { name = 'q',           mathmode = true, concealchar = '𝔮'},
    { name = 'eps',         mathmode = true, concealchar = 'ε'},
    { name = 'diff',        mathmode = true, concealchar = 'd'},
    { name = 'dl',          mathmode = true, concealchar = 'd'},
    { name = 'diffp',       mathmode = true, concealchar = '∂'},
    { name = 'e',           mathmode = true, concealchar = 'e'},
    { name = 'ii',          mathmode = true, concealchar = 'i'},
    { name = 'xrightarrow', mathmode = true, concealchar = '→'},
    { name = 'inj',         mathmode = true, concealchar = '↪'},
    { name = 'proj',        mathmode = true, concealchar = '↠'},
    { name = 'iso',         mathmode = true, concealchar = '⥲'},
    { name = 'underbrace',  mathmode = true, concealchar = '⏟'},
    { name = 'qedhere',     mathmode = true, concealchar = '□'},
    { name = 'qedhere',                      concealchar = '□'},
    { name = 'nbhyph',                       concealchar = '-'},
    { name = 'bhyph',                        concealchar = '='},
    { name = 'defemph',     conceal = true,  argstyle = 'bold'},
    { name = 'textsc',      conceal = true,  argstyle = 'bold'},
}
