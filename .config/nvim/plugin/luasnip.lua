require("luasnip").setup{
    history = false, -- Cannot jump back to already triggered snippets
    region_check_events = 'CursorMoved', -- Outside snippet if cursor moved
    delete_check_events = { 'TextChanged', 'TextChangedI' }, -- Delte jumps
    enable_autosnippets = true, -- Auto triggered snippets
    store_selection_keys = '<Tab>' -- Snippets with visual mode
}

require("luasnip.loaders.from_lua").load({ paths = "./snippet" })
