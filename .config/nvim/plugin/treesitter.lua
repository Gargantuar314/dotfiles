require('nvim-treesitter.configs').setup{
    ensure_installed = { -- Installed language parsers
        -- 'vimdoc' for vim doc, 'query' for tree-sitter files
        'lua', 'vim', 'vimdoc', 'query',
        'c', 'cpp', 'make', 'cmake',
        'rust',
        'python',
        'latex',
        'markdown', 'markdown_inline', -- Both experimental
    },

    sync_install = false, -- Install parsers synchronously
    auto_install = false, -- Do not autoinstall missing parsers

    highlight = {
        enable = true, -- Why does this option even exist?
        disable = { 'latex' },
        additional_vim_regex_highlighting = false, -- No vim syntax highlighting
    },
}
