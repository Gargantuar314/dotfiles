local function insert_wrap(_, parent)
    local raw = parent.env.LS_SELECT_RAW
    if #raw == 0 then -- Insert text if no selection
        return sn(nil, i(1))
    else -- Else insert selection
        return sn(nil, t(raw))
    end
end

return {}, { -- Autosnippets

s({
    trig = "(",
    dscr = "parenthesis",
    wordTrig = false
}, {
    t('('), d(1, insert_wrap), t(')')
}),

s({
    trig = "[",
    dscr = "brackets",
    wordTrig = false
}, {
    t('['), d(1, insert_wrap), t(']')
}),

s({
    trig = "{",
    dscr = "braces",
    wordTrig = false
}, {
    t('{'), d(1, insert_wrap), t('}')
}),

}
