local dracula = require('dracula')

dracula.setup{
    colors = { -- Higher contrast against background
        comment = dracula.colors().white,
        visual = dracula.colors().comment
    },
    show_end_of_buffer = true, -- Show tilde characters at end of file
    transparent_bg = true, -- Transparent background
    italic_comment = true -- Italic comments
}

vim.cmd("colorscheme dracula")
