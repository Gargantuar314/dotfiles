require("bluloco").setup{
    style = "dark", -- Dark mode
    transparent = true, -- Transparent backgroudn
    italics = true -- Italic comments
}

vim.cmd("colorscheme bluloco")
