require("lualine").setup{
    options = {
        theme = "auto" -- Color scheme
        -- "dracula-nvim", provided by "dracula.nvim"
    },
    tabline = {
        lualine_a = { "buffers" } -- Show buffers at the top
    }
}
