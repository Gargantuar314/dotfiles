local vk = vim.keymap.set
vim.g.maplocalleader = " "
vim.g.mapleader = " "

-- General -----------------------------------------------------------------{{{1

-- Fix cursor in middle of screen while scrolling
vk('n', '<C-u>', '<C-u>zz') 
vk('n', '<C-d>', '<C-d>zz')

-- Open folds while searching
vk('n', 'n', 'nzzzv') 
vk('n', 'N', 'Nzzzv')

-- Copy/paste to clipboard
vk({ 'n', 'x' }, '<leader>y', '"+y')
vk({ 'n', 'x' }, '<leader>p', '"+p')

vk({ 'n', 'x' }, 'x', '"_x') -- Ignore register for 'x'

-- Do not skip soft wrapped lines during vertical movement
vk({ 'n', 'x' }, 'j', 'gj')
vk({ 'n', 'x' }, 'k', 'gk')

-- Insert empty line
vk({ 'n', 'x' }, '<leader>o', 'o<Esc>')
vk({ 'n', 'x' }, '<leader>O', 'O<Esc>')

-- Move/indent lines in visual mode
vk('v', 'J', ':move `>+1<CR>gv=gv')
vk('v', 'K', ':move `<-2<CR>gv=gv')
vk('v', '<', '<gv')
vk('v', '>', '>gv')

-- Switch/delete buffer
vk('n', '<leader>bn', ':bnext<CR>')
vk('n', '<leader>bp', ':bprevious<CR>')
vk('n', '<leader>bd', ':bdelete<CR>')

-- Move cursor to other window
vk('n', '<leader>wh', '<C-w>h')
vk('n', '<leader>wj', '<C-w>j')
vk('n', '<leader>wk', '<C-w>k')
vk('n', '<leader>wl', '<C-w>l')

-- Resize windows
vk('n', '<C-Up>', '<C-w>-')
vk('n', '<C-Down>', '<C-w>+')
vk('n', '<C-Left>', '<C-w><')
vk('n', '<C-Right>', '<C-w>>')

-- Create new window and move to it
vk('n', '<leader>ws', '<C-w>s<C-w>j') -- split
vk('n', '<leader>wv', '<C-w>v<C-w>l') -- vsplit

-- Change directory to file in current buffer
vk('n', '<leader>cd', ':cd %:p:h<CR>')

-- Luasnip -----------------------------------------------------------------{{{1

-- Tab to trigger snippet or jump to next node
vim.cmd[[
    imap <silent><expr> <Tab> luasnip#expand_or_jumpable()
        \ ? '<Plug>luasnip-expand-or-jump' : '<Tab>'
]]
-- Pure Lua version, but more verbose, not true if this works
--vk("i", "<Tab>", function()
--    if ls.expand_or_jumpable() then
--        ls.expand_or_jump()
--    else
--        vim.api.nvim_feedkeys(
--            vim.api.nvim_replace_termcodes("<Tab>", true, false, true),
--            "n", false)
--    end
--end)
vk("s", "<Tab>", "ls.jump(1)<CR>")

-- Shift-tab to jump to previous node
--vk({ "i", "s" }, "<S-Tab>", "ls.jump(-1)<CR>")

-- Cycle through choice snippets
vim.cmd[[
    imap <silent><expr> <C-A> luasnip#choice_active()
        \ ? '<Plug>luasnip-next-choice' : '<C-A>'
]]
vim.cmd[[
    imap <silent><expr> <C-S> luasnip#choice_active()
        \ ? '<Plug>luasnip-prev-choice' : '<C-S>'
]]

-- LSP ---------------------------------------------------------------------{{{1
-- Global mappings.
-- See `:help vim.diagnostic.*` for documentation on any of the below functions
vk('n', '<space>e', vim.diagnostic.open_float)
vk('n', '[d', vim.diagnostic.goto_prev)
vk('n', ']d', vim.diagnostic.goto_next)
vk('n', '<space>q', vim.diagnostic.setloclist)

-- Use LspAttach autocommand to only map the following keys
-- after the language server attaches to the current buffer
vim.api.nvim_create_autocmd('LspAttach', {
    group = vim.api.nvim_create_augroup('UserLspConfig', {}),
        callback = function(ev)
        -- Enable completion triggered by <c-x><c-o>
        vim.bo[ev.buf].omnifunc = 'v:lua.vim.lsp.omnifunc'

        -- Buffer local mappings.
        -- See `:help vim.lsp.*` for documentation on any of the below functions
        local opts = { buffer = ev.buf }
        vk('n', 'gD', vim.lsp.buf.declaration, opts)
        vk('n', 'gd', vim.lsp.buf.definition, opts)
        vk('n', 'K', vim.lsp.buf.hover, opts)
        vk('n', 'gi', vim.lsp.buf.implementation, opts)
        vk('n', '<C-k>', vim.lsp.buf.signature_help, opts)
        vk('n', '<space>wa', vim.lsp.buf.add_workspace_folder, opts)
        vk('n', '<space>wr', vim.lsp.buf.remove_workspace_folder, opts)
        vk('n', '<space>wl', function()
            print(vim.inspect(vim.lsp.buf.list_workspace_folders()))
        end, opts)
        vk('n', '<space>D', vim.lsp.buf.type_definition, opts)
        vk('n', '<space>rn', vim.lsp.buf.rename, opts)
        vk({ 'n', 'v' }, '<space>ca', vim.lsp.buf.code_action, opts)
        vk('n', 'gr', vim.lsp.buf.references, opts)
        vk('n', '<space>f', function()
            vim.lsp.buf.format { async = true }
        end, opts)
    end,
})
